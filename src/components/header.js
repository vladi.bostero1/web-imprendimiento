import React from 'react';
import './header.css';

export default class Header extends React.Component{
    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-dark floating w-100">
                    <a className="navbar-brand" href="#">IntPower</a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"/>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            {/*<li className="nav-item active">*/}
                                {/*<a className="nav-link" href="#">Top <span*/}
                                    {/*className="sr-only">(current)</span></a>*/}
                            {/*</li>*/}
                            <li className="nav-item">
                                <a className="nav-link" href="#">Proyectos</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Sobre nosotros</a>
                            </li>
                            {/*<li className="nav-item dropdown">*/}
                                {/*<a className="nav-link dropdown-toggle" href="#" id="navbarDropdown"*/}
                                   {/*role="button" data-toggle="dropdown" aria-haspopup="true"*/}
                                   {/*aria-expanded="false">*/}
                                    {/*El Equipo*/}
                                {/*</a>*/}
                                {/*<div className="dropdown-menu" aria-labelledby="navbarDropdown">*/}
                                    {/*<a className="dropdown-item" href="#">Nosotros!</a>*/}
                                    {/*<a className="dropdown-item" href="#">Contacto</a>*/}
                                    {/*<div className="dropdown-divider"/>*/}
                                    {/*<a className="dropdown-item" href="#">Algo! :D</a>*/}
                                {/*</div>*/}
                            {/*</li>*/}
                            {/*<li className="nav-item">*/}
                                {/*<a className="nav-link disabled" href="#" tabIndex="-1"*/}
                                   {/*aria-disabled="true">Disabled</a>*/}
                            {/*</li>*/}
                        </ul>
                    </div>
                </nav>
            </div>
        )
    }
};
