import React, {Component} from 'react';
import Header from './components/header';
import Footer from './components/footer';
import './App.css'
class App extends Component {
    render() {
        return (
            <div>
                <div className='container-header'>
                    <Header />
                    <div className='top-banner'>

                    </div>
                </div>

                <Footer/>
            </div>
        );
    }
}

export default App;
